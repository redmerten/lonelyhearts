/**
 * Created by AndreaMerten on 3/20/18.
 */

import {GET_MATCHES} from './types'
import {GET_INITIAL} from './types'
import {filterList} from '../Utilities/computeAge'

import axios from 'axios'

export const getInitial =()=> async dispatch =>{
  const res = await axios.get(`https://randomuser.me/api/?results=10&nat=us`)
  //console.log('initialAction',res)
  if (res.error){
    console.log('error from api')
  }
  else
    dispatch({type:GET_INITIAL,payload:res.data.results})
}


//I initially thougtht I need to re-request data upon submittal of criteria vs filtering the initial 10
export const getMatches =(gender, startAge=18, endAge=90)=> async dispatch =>{
  //ageMin=18&ageMax=90&female=on //&results=50
  console.log('matchAction')
  const res = await axios.get(`https://randomuser.me/api/?gender=${gender}&nat=us&results=50`)
  console.log('matchAction',res.data)
  const filtered=filterList(res.data.results, startAge, endAge) //list, start, end
  dispatch({type:GET_MATCHES,payload:filtered}) //res.data.results
}


