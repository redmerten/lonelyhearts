/**
 * Created by AndreaMerten on 4/2/18.
 */
import {GET_INITIAL} from '../Actions/types'

export default function (state = {}, action) {
  switch (action.type) {
    case GET_INITIAL:
      return action.payload
    default:
      return state
  }
}