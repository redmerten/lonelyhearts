/**
 * Created by AndreaMerten on 3/20/18.
 */

import {combineReducers} from 'redux'  //action creators immediately return action+payload
import getMatchesReducer from './getMatchesReducer'
import getInitialReducer from './getInitialReducer'


//this is imported by main index.js
export default combineReducers({
  matches:getMatchesReducer,
  initial:getInitialReducer
})