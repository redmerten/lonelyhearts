/**
 * Created by AndreaMerten on 4/2/18.
 */
import {GET_MATCHES} from '../Actions/types'

export default function (state = {}, action) {
  switch (action.type) {
    case GET_MATCHES:
      return action.payload
    default:
      return state
  }
}