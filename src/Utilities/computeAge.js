/**
 * Created by AndreaMerten on 4/2/18.
 */

import moment from 'moment'

export const computeAge = (date) =>{
  return moment().diff(date, 'years');
}

export const checkStartYear = (begAge,date) =>{
  return moment().diff(date, 'years') >= begAge
}

export const checkEndYear = (endAge,date) =>{
  return moment().diff(date, 'years') <= endAge
}

export const filterList = (list, start=18, end=90, gender) =>{
  if (gender === 'any')
    return list.filter(e=>checkStartYear(start, e.dob) && checkEndYear(end, e.dob))
  else
    return list.filter(e=>checkStartYear(start, e.dob) && checkEndYear(end, e.dob) && e.gender===gender)
}