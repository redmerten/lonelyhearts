/**
 * Created by AndreaMerten on 4/2/18.
 */
import React from 'react'
import '../Stylesheets/styles.css'

const MatchCard = ({image, name, age, onClick}) =>{
  return(
    <li style={styles.li}>
      <img className="photo" src={image} alt=""/>
      <div style ={styles.nameAgeDiv}>
        {/*<span style={styles.span}>{name} {age}</span>*/}
        <span className="name lead" style={styles.span}>{name}</span>
        <span className="age lead" style={styles.span}>{age}</span>
      </div>
      <button onClick={onClick}>Contact</button>
    </li>
  )
}

export default MatchCard

const styles={
  li:{
    display:'flex',
    justifyContent:'space-between',
    alignItems:'center',
    height:'100px',
    width:'99%',
    margin: '2% 0%'
  },
  // photo:{
  //   borderRadius: '50%',
  //   border: '1px solid gray',
  //   background: 'gray',
  //   width: '100px',
  //   height: 'auto',
  // },
  nameAgeDiv:{
    width:'50%',
    display:'flex',
    justifyContent:'space-between',
    alignItems:'center',
    margin: '2% 0%'
  },
  // button:{
  //   width: '50px',
  //   height: '40px',
  // },
  span:{
    textTransform: 'capitalize'
  }
}