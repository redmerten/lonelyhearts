/**
 * Created by AndreaMerten on 4/3/18.
 */
import React from 'react'

const ContactDetail=({detail, onClick})=>{
  return(
    <div style={styles.div}>
      <p>phone: {detail.phone}</p>
      <p>cell: {detail.cell}</p>
      <p>email: {detail.email}</p>
      <button
        onClick={onClick}
        style={styles.button}
      >
        X
      </button>
    </div>
  )
}

export default ContactDetail

const styles={
  div:{
    width: '100%',
    height:'175px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent:'flex-start',
    alignItems:'center',
    margin:'2% 2% 2% 5%'
  },
  button:{
    alignSelf:'center',
    borderRadius:'50%',
    border:'1px solid black'
  }
}

//Phone # Cell # E-mail