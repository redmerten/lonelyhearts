/**
 * Created by AndreaMerten on 4/3/18.
 */
import React from 'react'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {  mount,  } from 'enzyme';

import {Landing} from './Landing'
import ContactDetail from './ContactDetail'
import MatchCard from './MatchCard'

configure({ adapter: new Adapter() });

const props = {
  getMatches : jest.fn(() => ({})),
  getInitial: jest.fn()
}

const wrapper = mount(<Landing {...props}/>)

describe('<Landing/>', () => {

  //Length 1 if Landing renders
  it ('Should render self and components', ()=>{
    expect(wrapper).toHaveLength(1)
    expect(wrapper.find(<ContactDetail/>))
    expect(wrapper.find(<MatchCard/>))
  })
});