/**
 * Created by AndreaMerten on 4/2/18.
 */
import React from 'react'

const Navbar =() =>{
  return(
      <div className="header" style={styles.header}>
        <h1>Lonely Hearts Dating Service</h1>
      </div>
    )
}

export default Navbar

const styles={
  header:{
    margin: '0px',
    //padding-bottom: 80px,
    color: 'white',
    background: 'gray',
    /*text-align: center,*/
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100px',
    width: '100%',
  }
  
}