/**
 * Created by AndreaMerten on 4/3/18.
 */
import React from 'react'
import MatchCard from './MatchCard'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import renderer from 'react-test-renderer'

configure({ adapter: new Adapter() });

describe('MatchCard Component Test', () => {
  const props = {
    //image, name, age, onClick
    onClick: jest.fn(),
    image:'',
    name:'',
    age:''
  }
  const Component = <MatchCard {...props}/>
  it('should match latest snapshot', ()=>{
    const snapshot = renderer.create(Component)
    expect(snapshot.toJSON()).toMatchSnapshot()
  })
})