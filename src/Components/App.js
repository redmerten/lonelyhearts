
import React, {Component} from 'react'
import {BrowserRouter, Route} from 'react-router-dom'

import '../Stylesheets/styles.css'

//app will show all components

//import Navbar from './Navbar'
import Landing from './Landing'
import Navbar from './Navbar'

class  App extends Component {

  render () {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Navbar/>
            <Route exact path='/' component={Landing}/>

          </div>
        </BrowserRouter>
      </div>
    )
  }
}

//this is imported by main index.js
export default App


