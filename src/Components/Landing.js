/**
 * Created by AndreaMerten on 3/22/18.
 */

import React, {Component} from 'react'
import {connect} from 'react-redux'
import MatchCard from './MatchCard'
import ContactDetail from './ContactDetail'
import {getMatches, getInitial} from '../Actions'
import {computeAge, filterList} from '../Utilities/computeAge'
import '../Stylesheets/styles.css'

import Radium from 'radium'

export class Landing extends Component{
  state={
    ageMin:undefined,
    ageMax:undefined,
    gender:'any',
    any:true,
    male:false,
    female:false,
    reset:true,
    filtered:[],
    showIndex:null
  }

  //get initial list
  componentDidMount(){
    this.props.getInitial()
  }

  componentDidUpdate(){

  }

  inputChange=(event,name)=>{
    console.log('input',name, event.target.value)
    this.setState({[name]:event.target.value})
  }

  genderSelection=(event) =>{
    const target = event.target
    const name = target.name
    this.setState({any:false, male:false, female:false, gender:name})
    this.setState({[name]:true})
  }

  findMatches = (event)=>{
    const {ageMin, ageMax, gender} = this.state
    event.preventDefault()
    const filtered=filterList(this.props.initial, ageMin, ageMax, gender) //list, start, end
    //console.log('findMatches', filtered)
    this.setState({reset:false, filtered})
    ////this.props.getMatches(gender,ageMin, ageMax)
  }

  onContactClick = (i) =>{
   // console.log(this.props.(i))
    this.setState({showIndex:i})
  }

  onClose = () =>{
    this.setState({showIndex:null})
  }

  onReset = (event)=>{
    event.preventDefault()
    this.setState({reset:true, filtered:[]})
  }

  renderMatches=(initial)=>{
    const {showIndex}=this.state
    return(
      <div id="searchResults">
        <h2 style={styles.candidates}>{initial.length} Candidates Found</h2>
        <ul>
          {/*<li>*/}
          {/*</li>*/}
          {initial.map((e,i)=>{
              return(
                <div key={i}>
                  <MatchCard
                    image={e.picture.thumbnail}
                    name={`${e.name.first} ${e.name.last}`}
                    age = {computeAge(e.dob.split(' ')[0])}
                    onClick={()=>this.onContactClick(i)}
                  />
                  {showIndex === i
                    ?
                  <ContactDetail
                    detail={e}
                    onClick={()=>this.onClose()}
                  />
                    :
                    <div/>
                  }
                </div>
              )
            })
          }
        </ul>
      </div>
    )
  }


  render(){
    //console.log('state',this.state)
    const {ageMin, ageMax, any, male, female, reset, filtered}=this.state
    const {initial} = this.props
    console.log('initial', initial)

    return(
      <div className="container text-center">
          <div id="userPreferences">
            <h2>Search Criteria</h2>
            <form className="form">
              <div className="form-group">
                <label className="control-label">Age</label>
                <input 
                  type="text" 
                  name="ageMin" 
                  value={ageMin}
                  placeholder={18}
                  onChange={(e)=>this.inputChange(e,'ageMin')}/>
                <label className="control-label">to</label>
                <input
                  type="text"
                  name="ageMax"
                  value={ageMax}
                  placeholder={90}
                  onChange={(e)=>this.inputChange(e,'ageMax')}/>
              </div>

              <div className="form-group">
                <label className="control-label">Gender</label>
                <label className="radio-inline">
                  <input
                    type="radio"
                    name="any"
                    checked={any}
                    onChange={(e)=>this.genderSelection(e)}
                  />
                  Any
                </label>
                <label className="radio-inline">
                  <input
                    type="radio"
                    name="male"
                    checked={male}
                    onChange={(e)=>this.genderSelection(e)}
                  />
                  Male
                </label>
                <label className="radio-inline">
                  <input
                    type="radio"
                    name="female"
                    checked={female}
                    onChange={(e)=>this.genderSelection(e)}
                  />
                  Female
                </label>
              </div>

              <button
                type="button"
                className="btn btn-default"
                onClick={(e)=>this.onReset(e)}

              >
                Reset
              </button>
              <button
                type="submit"
                className="btn btn-primary"
                onClick={(e)=>this.findMatches(e)}
              >
                Filter
              </button>
            </form>
          </div>
        {reset && Array.isArray(initial)
          ?
          this.renderMatches(initial)
          :
          <div/>
        }
        {!reset
          ?
          this.renderMatches(filtered)
          :
          <div/>
        }



      </div>
    )
  }
}

const mapStateToProps = (state) =>{
  return state
}

export default connect(mapStateToProps, {getMatches, getInitial})(Landing)

const styles={
  div:{
    width:'100%',
    display:'flex',
    flexDirection:'column',
    justifyContent: 'center',
    alignItems:'center',
    margin:'2%'
  },
  candidates:{
    margin:'10%, 0% 2% 0%'
  },
  searchResults:{
    background: 'aqua',
    margin: '2% 0%',
    width: '50%',
    borderRadius: '20px',
    padding: '1%',
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
  }
}